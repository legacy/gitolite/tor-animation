1
00:00:00,660 --> 00:00:02,780
Przyzwyczailiśmy się do internetu.

2
00:00:03,120 --> 00:00:06,000
Stale dzielimy się informacjami o naszym życiu prywatnym:

3
00:00:06,300 --> 00:00:08,260
o tym co jemy, z kim się spotykamy,

4
00:00:8,600 --> 00:00:10,480
miejsca, które odwiedzamy i co czytamy.

5
00:00:11,280 --> 00:00:12,640
Pozwólcie, że wyjaśnię.

6
00:00:13,620 --> 00:00:15,740
Właśnie w tym momencie,
ktoś próbuje Cię podejrzeć,

7
00:00:15,860 --> 00:00:20,480
zobaczy Twoją prawdziwą tożsamość,
dokładną lokalizację i system operacyjny,

8
00:00:20,600 --> 00:00:24,500
wszystkie strony, które odwiedziłeś,
przeglądarkę której używasz,

9
00:00:24,600 --> 00:00:27,340
i o wiele więcej informacji
o Tobie i Twoim życiu,

10
00:00:27,420 --> 00:00:29,500
których wolałbyś nie
dzielić z nieznajomymi osobami,

11
00:00:29,650 --> 00:00:34,000
które mogą użyć tych danych,
aby Cię skrzywdzić, albo na nich zarobić.

12
00:00:34,500 --> 00:00:37,220
Ale nie jeśli używasz Tor!

13
00:00:37,700 --> 00:00:41,800
Tor Browser chroni Twoją prywatność
i tożsamość w Internecie.

14
00:00:42,560 --> 00:00:44,760
Tor zabezpiecza Twoje połączenie
poprzez trzy warstwy szyfrowania

15
00:00:44,940 --> 00:00:49,760
i przesyła je przez trzy serwery
udostępnione przez wolontariuszy na całym świecie.

16
00:00:50,280 --> 00:00:53,520
W ten sposób możemy komunikować się
anonimowo przez Internet.

17
00:00:56,560 --> 00:00:58,280
Tor chroni również nasze dane

18
00:00:58,400 --> 00:01:01,900
przed korporacjami lub rządem
i masową inwigilacją.

19
00:01:02,880 --> 00:01:07,340
Być może żyjesz w represjonowanym kraju,
który próbuje kontrolować i nadzorować Internet.

20
00:01:07,900 --> 00:01:11,800
Albo nie chcesz, aby wielkie korporacje
wykorzystywały Twoje osobiste dane.

21
00:01:12,880 --> 00:01:15,640
Tor sprawia, że wszyscy jego użytkownicy
wyglądają tak samo

22
00:01:15,920 --> 00:01:18,800
co myli obserwatora
i daje Ci anonimowość.

23
00:01:19,500 --> 00:01:22,980
A więc, im więcej osób używa sieci Tor,
tym silniejsza się ona staje

24
00:01:23,140 --> 00:01:27,800
jako, że łatwiej jest się ukryć w tłumie
ludzi, którzy wyglądają dokładnie tak samo.

25
00:01:28,700 --> 00:01:31,240
Możesz ominąć cenzurę
bez obaw o to, że

26
00:01:31,400 --> 00:01:34,100
cenzorzy zobaczą co robisz
w Internecie.

27
00:01:36,540 --> 00:01:39,440
Reklamy nie będą Cię prześladować
przez cały czas,

28
00:01:39,500 --> 00:01:41,300
odkąd po raz pierwszy
w nie kliknąłeś.

29
00:01:42,880 --> 00:01:47,380
Używając Tor, strony, które odwiedzasz
nie będą wiedziały kim jesteś,

30
00:01:47,540 --> 00:01:49,760
a nawet z jakiej części świata
je odwiedzasz,

31
00:01:49,920 --> 00:01:51,920
o ile nie zalogujesz się i im tego nie powiesz.

32
00:01:54,200 --> 00:01:55,840
Ściągając i używając Tor,

33
00:01:56,200 --> 00:01:58,560
możesz ochronić ludzi,
którzy potrzebują anonimowości,

34
00:01:58,880 --> 00:02:01,640
jak aktywiści, dziennikarze i blogerzy.

35
00:02:02,000 --> 00:02:07,000
Pobierz i używaj Tor, lub uruchom własny przekaźnik pod tym adresem.

36
00:02:14,400 --> 00:02:17,200
Tłumaczone przez Hoek i Nati, edycja: missq.

