1
00:00:00,660 --> 00:00:02,780
Ens hem acostumat molt a internet. 

2
00:00:03,120 --> 00:00:07,700
Estem compartint informació constantment
sobre nosaltres i la nostra vida privada:

3
00:00:08,000 --> 00:00:09,960
el menjar que mengem, la gent que coneixem,

4
00:00:10,180 --> 00:00:12,480
els llocs on anem i les coses que llegim.

5
00:00:13,280 --> 00:00:14,640
Deixa que m'expliqui millor.

6
00:00:14,920 --> 00:00:17,740
En aquest precís moment, 
si algú intenta localitzar-vos,

7
00:00:18,060 --> 00:00:22,480
veurà la vostra identitat real, 
la localització precisa, el sistema operatiu, 

8
00:00:22,800 --> 00:00:26,500
tots els llocs web que heu visitat, 
el navegador que useu per navegar per internet, 

9
00:00:26,700 --> 00:00:29,140
i molta més informació
sobre vosaltres i la vostra vida. 

10
00:00:29,200 --> 00:00:31,500
la qual probablement no teníeu intenció
de compartir amb desconeguts,

11
00:00:31,700 --> 00:00:34,000
que podrien fer servir fàcilment aquesta informació
per abusar de vosaltres.

12
00:00:34,500 --> 00:00:37,000
Però això no passa si esteu fent servir Tor!

13
00:00:37,140 --> 00:00:40,840
El navegador Tor protegeix la nostra privadesa
i la nostra identitat a Internet. 

14
00:00:41,560 --> 00:00:44,760
Tor assegura la teva connexió
amb tres capes d'encriptatge.

15
00:00:44,940 --> 00:00:49,760
i la passa per tres servidors duts a terme
voluntàriament a tot el món, 

16
00:00:50,280 --> 00:00:53,520
que ens permeten comunicar-nos
de manera anònima per Internet. 

17
00:00:56,560 --> 00:00:58,280
Tor també protegeix la nostra informació

18
00:00:58,400 --> 00:01:01,900
dels objectius del govern o les grans empreses
i la vigilància massiva. 

19
00:01:02,880 --> 00:01:07,340
Potser viviu en un país repressiu
que intenta controlar i vigilar Internet.

20
00:01:07,900 --> 00:01:11,800
O potser no voleu que les grans empreses
s'aprofitin de la vostra informació personal. 

21
00:01:12,880 --> 00:01:15,640
Tot fa que tots els seus usuaris
siguin iguals

22
00:01:15,920 --> 00:01:18,800
cosa que confon l'observador
i us fa anònims.

23
00:01:19,500 --> 00:01:22,980
Per tant, quanta més gent faci servir la xarxa Tor
més forta es farà

24
00:01:23,140 --> 00:01:27,800
ja que és més fàcil amagar-se en una multitud
de gent que és idèntica.

25
00:01:28,700 --> 00:01:31,240
Podeu sortejar la censura
sense preocupar-vos que

26
00:01:31,400 --> 00:01:34,100
el censor sàpigue el que feu
a internet. 


27
00:01:36,540 --> 00:01:39,440
Els anuncis no us seguiran
allà on aneu durant mesos,

28
00:01:39,640 --> 00:01:41,300
començant per quan vau fer
clic per primer cop en un producte.

29
00:01:43,880 --> 00:01:47,380
Fent servir Tor, els llocs que visiteu
no sabran ni qui sou, 

30
00:01:47,540 --> 00:01:49,760
ni de quina part del món
els esteu visitant

31
00:01:49,920 --> 00:01:51,920
a no ser que us identifiqueu i els ho digueu.

32
00:01:54,200 --> 00:01:55,840
Baixant i usant Tor

33
00:01:56,200 --> 00:01:58,560
podeu protegir la gent
que necessita anonimat.

34
00:01:58,880 --> 00:02:01,640
com els activistes, periodistes i bloggers.

35
00:02:02,000 --> 00:02:07,000
Baixa i utilitza Tor! O executa un relé!

