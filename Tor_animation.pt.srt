1
00:00:00,660 --> 00:00:02,780
Habituámo-nos à Internet.

2
00:00:03,120 --> 00:00:07,700
Passamos o tempo a partilhar informação
sobre nós mesmos e as nossas vidas:

3
00:00:08,000 --> 00:00:09,960
o que comemos, quem conhecemos,

4
00:00:10,180 --> 00:00:12,480
onde vamos, e o que lemos.

5
00:00:13,280 --> 00:00:14,640
Deixem-me explicar melhor.

6
00:00:14,920 --> 00:00:17,740
Se neste preciso momento alguém
tentar investigar a tua ligação,

7
00:00:18,060 --> 00:00:22,480
vão ver a tua identidade real,
a tua localização, sistema operativo,

8
00:00:22,800 --> 00:00:26,500
todos os sites que visitaste,
o browser que usas,

9
00:00:26,700 --> 00:00:29,140
e muita mais informação
sobre ti e a tua vida

10
00:00:29,200 --> 00:00:31,500
do que alguma vez pensaste partilhar
com pessoas desconhecidas,

11
00:00:31,700 --> 00:00:34,000
que podem tentar usar essa informação
para te explorar.

12
00:00:34,500 --> 00:00:37,000
Mas não se usares o Tor!

13
00:00:37,140 --> 00:00:40,840
O Tor Browser protege a tua
identidade e privacidade online.

14
00:00:41,560 --> 00:00:44,760
O Tor protege a tua ligação com
três niveís de encriptação

15
00:00:44,940 --> 00:00:49,760
e passa-a por três servidores voluntários
em todo o mundo,

16
00:00:50,280 --> 00:00:53,520
o que te pemite comunicar anonimamente
através da internet.

17
00:00:56,560 --> 00:00:58,280
O Tor também protege os teus dados

18
00:00:58,400 --> 00:01:01,900
contra empresas e governos que praticam
espionagem em massa na internet.

19
00:01:02,880 --> 00:01:07,340
Talvez vivas num país ditatorial
que tenta controlar e vigiar a Internet.

20
00:01:07,900 --> 00:01:11,800
Ou talvez não queiras que as grandes empresas
tirem partido da tua informação pessoal.

21
00:01:12,880 --> 00:01:15,640
O Tor faz com que todos os seus utilizadores
pareçam ser iguais

22
00:01:15,920 --> 00:01:18,800
o que confunde quem te observa
e te torna anónimo.

23
00:01:19,500 --> 00:01:22,980
Assim, quantas mais pessoas usam a rede Tor,
mais forte ela se torna

24
00:01:23,140 --> 00:01:27,800
pois é mais fácil esconder-se
numa multidão de pessoas iguais.

25
00:01:28,700 --> 00:01:31,240
Podes ultrapassar a censura

26
00:01:31,400 --> 00:01:34,100
sem te preocupares se quem te observa
sabe o que fazes na internet.

27
00:01:36,540 --> 00:01:39,440
A publicidade não te vai seguir
durante meses,

28
00:01:39,640 --> 00:01:41,300
assim que clicas num produto
pela primeira vez.

29
00:01:43,880 --> 00:01:47,380
Ao usares o Tor, os sites que visitas
não vão saber quem és,

30
00:01:47,540 --> 00:01:49,760
em que parte do mundo estás,

31
00:01:49,920 --> 00:01:51,920
a não ser que te registas
e lhes digas isso tu mesmo.

32
00:01:54,200 --> 00:01:55,840
Ao usarem o Tor,

33
00:01:56,200 --> 00:01:58,560
pessoas que precisam de anonimato
podem proteger-se,

34
00:01:58,880 --> 00:02:01,640
como activistas, jornalistas e bloggers.

35
00:02:02,000 --> 00:02:07,000
Faz download e usa Tor!
Ou providencia um servidor voluntário!

36
00:02:12,400 --> 00:02:15,200
Traduzido por “Sir Anthony Dorian Gray”

